#include "test.h"
#include "mem.h"
#include "mem_internals.h"
#include <stdio.h>

static void test_start_message(int test_num){
    fprintf(stdout, "\n===RUNNING TEST №%i===\n", test_num);
}

static void test_failed_message(int test_num, char* error){
    fprintf(stderr, "\n===TEST №%i: FAILED===\n", test_num);

    fprintf(stderr, error, test_num);
}

static void test_success_message(int test_num){
    fprintf(stdout, "\n===TEST №%i: PASSED===\n", test_num);
}

static inline void destroy_heap(void* heap, size_t size) {
  munmap(heap, size_from_capacity((block_capacity) {.bytes = size}).bytes);
}

static inline struct block_header* find_header(void* contents) {
    return (struct block_header*) ((uint8_t*) contents - offsetof(struct block_header, contents));
}
//  Обычное успешное выделение памяти.

void run_first_test() {
  int test_num = 1;
  test_start_message(test_num);

  void* heap = heap_init(8*KB);
  printf("Initialized heap:\n");
  debug_heap(stdout, heap);

  void* allocation = _malloc(4*KB);
  printf("Heap after malloc:\n");
  debug_heap(stdout, heap);
  if (!heap || !allocation) {
    test_failed_message(test_num, heap ? "Error in malloc\n" : "Error in heap_init\n");
    return;
  }

  _free(allocation);
  printf("Heap after releasing:\n");
  debug_heap(stdout, heap);

  destroy_heap(heap, 8*KB);
  test_success_message(test_num);
}

// Освобождение одного блока из нескольких выделенных.

void run_second_test(){
    int test_num = 2;
    test_start_message(test_num);

    void* heap = heap_init(8*KB);
    printf("Initialized heap:\n");
    debug_heap(stdout, heap);

    void* first_allocation = _malloc(128);
    void* second_allocation = _malloc(64);
    printf("Heap after mallocs:\n");
    debug_heap(stdout, heap);
    if (!heap || !first_allocation || !second_allocation) {
        test_failed_message(test_num, heap ? "Error in malloc\n" : "Error in heap_init\n");
        return;
    }

    _free(first_allocation);
    _free(second_allocation);
    printf("Heap after releasing:\n");
    debug_heap(stdout, heap);

    destroy_heap(heap, 8*KB);
    test_success_message(test_num);

}

// Освобождение двух блоков из нескольких выделенных

void run_third_test(){
    int test_num = 3;
    test_start_message(test_num);

    void* heap = heap_init(8*KB);
    printf("Initialized heap:\n");
    debug_heap(stdout, heap);

    void* first_allocation = _malloc(128);
    void* second_allocation = _malloc(64);
    void* third_allocation = _malloc(64);
    printf("Heap after mallocs:\n");
    debug_heap(stdout, heap);
    if (!heap || !first_allocation || !second_allocation || !third_allocation) {
        test_failed_message(test_num, heap ? "Error in malloc\n" : "Error in heap_init\n");
        return;
    }

    _free(first_allocation);
    _free(second_allocation);
    _free(third_allocation);
    printf("Heap after releasing:\n");
    debug_heap(stdout, heap);

    destroy_heap(heap, 8*KB);
    test_success_message(test_num);

}

// Память закончилась, новый регион памяти расширяет старый

void run_fourth_test(){
    
    int test_num = 4;
    test_start_message(test_num);

    void* heap = heap_init(1);
    printf("Initialized heap:\n");
    debug_heap(stdout, heap);

    void* first_allocation = _malloc(16*KB);
    void* second_allocation = _malloc(16*KB);
    printf("Heap after mallocs:\n");
    debug_heap(stdout, heap);
    struct block_header* first_header = find_header(first_allocation);
    if (!heap || !first_allocation || !second_allocation) {
        test_failed_message(test_num, heap ? "Error in malloc\n" : "Error in heap_init\n");
            return;
    }
    if(first_header->next != find_header(second_allocation)){
        test_failed_message(test_num, "Error in headers\n");
        return;
    }

    _free(first_allocation);
    _free(second_allocation);
    printf("Heap after releasing:\n");
    debug_heap(stdout, heap);

    destroy_heap(heap, 16*KB);
    test_success_message(test_num);

}
 
// Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте.

void run_fifth_test(){
    int test_num = 5;
    test_start_message(test_num);

    void* heap = heap_init(1);
    printf("Initialized heap:\n");
    debug_heap(stdout, heap);

    void* first_allocation = _malloc(16*KB);
    struct block_header* first_header = find_header(first_allocation);
    if (!heap || !first_allocation) {
        test_failed_message(test_num, heap ? "Error in malloc\n" : "Error in heap_init\n");
        return;
    }

    (void) mmap(first_header->contents + first_header->capacity.bytes, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, -1, 0);

    void* second_allocation = _malloc(16*KB);
    printf("Heap after mallocs:\n");
    debug_heap(stdout, heap);
    if (!second_allocation || find_header(second_allocation) == (void*) (first_header->contents + first_header->capacity.bytes)) {
        test_failed_message(test_num, second_allocation ? "Error in headers\n" : "Error in malloc\n");
        return;
    }

    _free(first_allocation);
    _free(second_allocation);
    printf("Heap after releasing:\n");
    debug_heap(stdout, heap);

    destroy_heap(heap, 16384);
    test_success_message(test_num);

}
void run_tests(){
    run_first_test();
    run_second_test();
    run_third_test();
    run_fourth_test();
    run_fifth_test();
}